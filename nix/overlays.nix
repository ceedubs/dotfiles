{ unison-nix, nixpkgs-unstable }:

let

  # additions/modifications that are general and could reasonably go into the main nixpkgs repo
  standardOverlay = self: super:
    {
    };

  unisonOverlay = unison-nix.overlay;

  # additions/modifications that are fairly specific to me, such as baking in my config files, etc.
  customizationOverlay = self: super:
    {
      gnupg = super.gnupg.override {
        guiSupport = false;
        pinentry = self.pinentry;
      };

      vimPlugins = super.vimPlugins // {
        inherit (nixpkgs-unstable.vimPlugins ) copilot-vim;
      };

      customized = {

        pinentry = if (self.stdenv.isDarwin) then self.pinentry_mac else self.pinentry-curses;

        tmux = self.callPackage ./tmux { inherit (super) tmux; };

        fzf = self.callPackage ./fzf { inherit (super) fzf; };

        vim = self.callPackage ./vim {
          inherit (self.nodePackages) bash-language-server dockerfile-language-server-nodejs;
        };

        nix-direnv = super.nix-direnv.override { enableFlakes = true; };

        wemux = super.wemux.override {
          inherit (self.customized) tmux;
        };
      };
    };

in
[ standardOverlay unisonOverlay customizationOverlay ]
