{ pinentry }:

''
pinentry-program ${pinentry}/bin/pinentry

# Set the default cache time to 7 days.
default-cache-ttl       604800
default-cache-ttl-ssh   604800

# Set the max cache time to 30 days.
max-cache-ttl           2592000
max-cache-ttl-ssh       2592000
''
