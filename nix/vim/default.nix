# Vim, with a set of extra packages (extraPackages) and a custom vimrc
{ bash-language-server, dhall-lsp-server, dockerfile-language-server-nodejs, neovim, nodejs, terraform-ls, vimPlugins, makeWrapper, rnix-lsp, symlinkJoin, writeText }:

let
  # setting g:coc_node_path can be removed if the Nix derivation for coc.nvim is changed to set the
  # node path such as with https://github.com/NixOS/nixpkgs/pull/91447
  dynamicRc = ''
    let g:coc_node_path='${nodejs}/bin/node'
    let g:copilot_node_command='${nodejs}/bin/node'
    let &runtimepath.=','.'${./rumtime}'
  '';

  coc-settings = import ./coc-nvim/coc-settings.nix {
    inherit bash-language-server dhall-lsp-server dockerfile-language-server-nodejs rnix-lsp terraform-ls;
  };

  coc-settings-file = writeText "coc-settings.json" (builtins.toJSON coc-settings);

  neovim-unwrapped = neovim.override {
    vimAlias = false;
    configure = {
      customRC = dynamicRc + "\n" + builtins.readFile ./vimrc;
      packages.myVimPackage = with vimPlugins; {
        start = [
          coc-nvim
          copilot-vim
          dhall-vim
          fzf-vim
          fzfWrapper
          indentLine
          rainbow
          rust-vim
          tabular
          vim-abolish
          vim-airline
          vim-airline-themes
          vim-easy-align
          vim-easymotion
          vim-eunuch
          vim-fugitive
          vim-hcl
          vim-indent-object
          vim-markdown
          vim-nix
          vim-ormolu
          vim-repeat
          vim-rhubarb
          vim-scala
          vim-shellcheck
          vim-surround
          vim-test
          vim-unison
          vimux
        ];
      };
    };
  };
in
  symlinkJoin {
    name = "nvim";
    buildInputs = [ makeWrapper ];
    paths = [ neovim-unwrapped ];
    postBuild = ''
      mkdir -p $out/conf
      cp ${coc-settings-file} $out/conf/coc-settings.json
      wrapProgram "$out/bin/nvim" \
        --set VIMCONFIG "$out/conf"
      makeWrapper "$out/bin/nvim" "$out/bin/vim"
    '';
  }
