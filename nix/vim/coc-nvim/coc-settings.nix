{ bash-language-server, dhall-lsp-server, dockerfile-language-server-nodejs, rnix-lsp, terraform-ls }:

{
  coc.preferences = {
    extensionUpdateCheck = "never";
    codeLens.enable = true;
    #diagnostic.virtualText = true;
    diagnostic.errorSign = "🚽";
  };

  languageserver = {
    bash = {
      command = bash-language-server + "/bin/bash-language-server";
      args = [ "start" ];
      filetypes = [ "sh" ];
      ignoredRootPaths = [ "~" ];
    };
    haskell = {
      command = "haskell-language-server-wrapper";
      args = [ "--lsp" ];
      rootPatterns = [ "*.cabal" "stack.yaml" "cabal.project" "package.yaml" "hie.yaml" ];
      filetypes = [ "haskell" "lhaskell" ];
    };
    dhall = {
      command = dhall-lsp-server + "/bin/dhall-lsp-server";
      filetypes = [ "dhall" ];
    };
    dockerfile = {
      command = dockerfile-language-server-nodejs + "/bin/docker-langserver";
      filetypes = [ "dockerfile" ];
      args = [ "--stdio" ];
    };
    nix = {
      command = rnix-lsp + "/bin/rnix-lsp";
      filetypes = [ "nix" ];
    };
    terraform = {
      command = terraform-ls + "/bin/terraform-ls";
      args = [ "serve" ];
      filetypes = [ "terraform" "tf" ];
      initializationOptions = { };
      settings = { };
    };
    unison = {
      filetypes = [ "unison" ];
      host = "127.0.0.1";
      port = 5757;
    };
  };

  suggest = {
    autoTrigger = "trigger";
    noselect = false;
    enablePreview = true;
  };
}
