{
  description = "Home Manager configuration of ceedubs";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs
    nixpkgs.url = "github:nixos/nixpkgs/release-22.05";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    home-manager.url = "github:nix-community/home-manager/release-22.05";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    unison-nix.url = "github:ceedubs/unison-nix";
    unison-nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { home-manager, unison-nix, nixpkgs-unstable, ... }:
    let
      username = "cody";

      common = {
        inherit username;

        # Update the state version as needed.
        # See the changelog here:
        # https://nix-community.github.io/home-manager/release-notes.html#sec-release-21.05
        stateVersion = "21.11";

        # Optionally use extraSpecialArgs
        # to pass through arguments to home.nix
        extraSpecialArgs = {
          inherit unison-nix;
        };

        extraModules = [
          ({ pkgs, ... }: rec {
            _module.args.nixpkgs-unstable = import nixpkgs-unstable { inherit (pkgs) system; };
          })
        ];

        configuration = import ./home.nix;
      };

      configs = {
        macbook = {
          system = "x86_64-darwin";
          homeDirectory = "/Users/${username}";
        };

        harlan = {
          system = "x86_64-linux";
          homeDirectory = "/home/${username}";
        };
      };

      toConfig = name: value:
        home-manager.lib.homeManagerConfiguration (common // value);

    in {
      homeConfigurations = builtins.mapAttrs toConfig configs;
    };
}
