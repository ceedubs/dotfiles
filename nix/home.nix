{ pkgs, unison-nix, nixpkgs-unstable, ... }:

{
  nixpkgs.overlays = import ./overlays.nix {
    inherit unison-nix nixpkgs-unstable;
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    enableSyntaxHighlighting = true;

    initExtra = builtins.readFile ./zsh/.zshrc;
  };

  xdg.configFile."git/config".text = import ./git/config.nix {
    inherit (pkgs) gnupg;
  };
  xdg.configFile."git/ignore".source = ./git/ignore;
  home.file.".gnupg/gpg-agent.conf".text = import ./gpg/gpg-agent.conf.nix {
    inherit (pkgs) pinentry;
  };
  home.file.".haskeline".text = ''
    maxHistorySize: Just 2000
    historyDuplicates: IgnoreAll
    editMode: Vi
  '';

  home.packages =
    with pkgs;
    with customized;
    [
      fzf
      tmux
      unison-ucm
      vim


      ansible
      ansible-lint
      consul
      nomad
      terraform
      vault
      asciinema
      awscli2
      bat
      cacert
      cachix
      consul
      ddgr
      dhall
      dhall-json
      direnv
      gawk
      git
      gnupg
      httpie
      gitAndTools.hub
      gitAndTools.lab
      nixpkgs-unstable.haskell-language-server
      jq
      keychain
      less
      nix-direnv
      packer
      pinentry
      pure-prompt
      ripgrep
      rnix-lsp
      shellcheck
      tree
      watchexec
      wemux
    ];
}
