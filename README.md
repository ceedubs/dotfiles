# my config files

# MOVED to [github](https://github.com/ceedubs/dotfiles)

The fact that GitLab was ready to roll out a change that would **DELETE** repositories without churn makes me not trust them as a source repository.

## setting up a new environment

### install nix

[Install Nix](https://nixos.org/nix/download.html)

### clone this repo and run the nix derivation

```sh
mkdir ~/code
cd code
nix-shell -p git --run 'git clone http://gitlab.com/ceedubs/dotfiles.git'
cd dotfiles
```

### activate home-manager config

Below, replace `harlan` with relevant config for the machine that you are running this on.

```sh
nix build --no-link ./nix#homeConfigurations.harlan.activationPackage
"$(nix path-info ./nix#homeConfigurations.harlan.activationPackage)"/activate
```

Once you have done this the first time and `home-manager` is installed, you can simplify the above to:

```sh
home-manager switch --flake ./nix#harlan
```

### apply my preferred settings (optional)

```sh
. scripts/init_env.sh
```

### add zsh to the allowed shells and change to it

```sh
echo "$HOME/.nix-profile/bin/zsh" | sudo tee -a /etc/shells

chsh -s "$HOME/.nix-profile/bin/zsh"
```

### import terminal settings

In Terminal settings, import `~/code/dotfiles/resources/cody-mac-terminal-settings.terminal` and make it the default profile.

## thanks

Thank you and credit to @nmattia for [homies](https://github.com/nmattia/homies/blob/master/tmux/default.nix), which got me started down the path of this dotfiles repo.
